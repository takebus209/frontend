$(".submit").on("click", function(event){
  event.preventDefault();
  var firstName = $(".search-content").val();
  var url = 'http://support.takebus.com/support/user/searchByFirstName?firstName='+firstName ;
  $.ajax({
    method:"GET",
    url: url,
    dataType: 'json',
    success: function(res){
      console.log(res);
      /*
      $('<table>').text(traverser(res)).appendTo(".show-result");
      */
      $.getJSON(res, function(data){
        var tr = $("<tr>");
        loop(data, tr);
        tr.appendTo(".show-result");
      });
    },
    error: function(res){
    }
  })
})

function traverser(object){
  for(var property in object){ 
      var row="<tr>";
    //check if the property itself is a object
    if(object[property] instanceof Object){  
      //if its object then iterate all its properties
      for(var prop in object[property]){ 
        row+="<td>"+object[property][prop]+"</td>";
        }
    }
    else   //if just a simple property
      row+="<td>"+object[property]+"</td>";   
      row+="</tr>";
  
    //append to a table  
    document.getElementById('result-table').innerHTML+=row;
  }
}


// obj is the object to loop, tr is the tr to append tds to
function loop(obj,tr) {
    $.each(obj, function(key, val) {
        if(val && typeof val === "object") { // object, call recursively
            var tr2 = $("<tr>").appendTo(
                $("<td>").appendTo(tr)
            );

            loop(val, tr2);
        } else {
            $("<td>", {
                id: key
            }).text(val).appendTo(tr);
        }
    });
}
/*
$("#search.firstname").on("click", searchByFirstName)

var searchByFirstName = function {
	fname = $scope.firstname;

}

$.ajax({
  method:"POST",
  contentType:"application/json",
  //url:"http://dev.wifi.takebus.com/linkedroad-user/user/wifiUserRegister",
  url: 'http://rest.learncode.academy/api/johnbob/friends',
  //url: 'http://localhost:1337/api',
  data: wifiDataString
}).done(function(res){
  console.log(res);
  // return of res:
  // Object {email: "fghjkl@fghjk.fghj", phonenumber: "12345", promo: "false", id: "57006e4855adfd01003e32a6"}
});
console.log(wifiDataString);
// return of wifiDataString:
// {"email":"test2@ghjk.com","phonenumber":"456","promo":"true"}
status = true;*/